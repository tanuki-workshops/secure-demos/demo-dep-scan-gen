# Dependency Scanning Demo

## Initialize the project
> if needeed: if there is no `package.json` and `yarn.lock` files at the root of the repository
```bash
yarn add fastify@2.14.1
yarn add fastify-static@2.0.0
```


## Add another dependency
> for demoing with a merge request
```bash
yarn add axios@0.21.0
```

## Generated `package.json`

You can find the various `package.json` files for the demo in the `/tmp` directory

